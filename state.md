#### 介绍
---

直接使用，支持自定义面板标题，自定义面板内容，支持手风琴模式

###### 在页面中使用组件
```
<zh-collapse @change='change' accordion>
	<zh-collapse-item v-for="item in list" :title='item.title' :content="item.content"
		@clickContent='clickContent'>
			<!-- <view class="" slot='title'>支持定义面板标题</view>
			<view class="" slot='cont'>支持定义内容</view> -->
	</zh-collapse-item>
</zh-collapse>
```
#### 参数说明
---
zh-collapse Props

| 参数			   	| 类型		| 默认值| 描述									|
| ---				| ---		| ---	| ---								  |
| accordion			| Boolean	| false	| 是否手风琴模式					    |
| border			| Boolean	| true	| 是否显示边框线       					|
| default_unfold	| Boolean	| []	| 默认展开面板(数组填写需要展开面板的下标)|


Slot 插槽

| 参数	| 说明			|
| ---	| ---			|
| title	| 自定义标题内容|
| cont	| 自定义展开内容|


zh-collapse Event
注意：请在 ```<zh-collapse></zh-collapse>```上监听此事件

|  事件名	| 说明											| 回调参数	|
| ---		| ---											| ---		|
| change	| 当前激活面板展开时触发(参数为激活面板的index)		| Array		|


zh-collapse-item Event
注意：请在 ```<zh-collapse-item></zh-collapse-item>```上监听此事件

|  事件名		| 说明					| 回调参数	|
| ---			| ---					| ---		|
| clickContent	| 当前激活面板的index		|  Number	|

##### 示例

使用了uview的箭头图标，没有安装uview可自行更换

```
<template>
	<view class="">
		<zh-collapse @change='change' accordion :default_unfold='[1,2]' >
			<zh-collapse-item v-for="item in list" :title='item.title' :content="item.content"
				@clickContent='clickContent'>
				<!-- <view class="" slot='title'>支持定义面板标题</view>
				<view class="" slot='cont'>支持定义内容</view> -->
			</zh-collapse-item>
		</zh-collapse>
	</view>
</template>

<script>
	export default {
		data() {
			return {
				list: [{
						id: 1,
						title: '面板标题1',
						content: '面板展开1内容'
					},
					{
						id: 2,
						title: '面板标题2',
						content: '面板展开2内容'
					},
					{
						id: 3,
						title: '面板标题3',
						content: '面板展开3内容'
					},
					{
						id: 4,
						title: '面板标题4',
						content: '面板展开4内容'
					}
				]
			}
		},
		methods: {
			change(e) {
				console.log(e);
			},
			clickContent(e) {
				console.log(e);
			},
		}
	}
</script>

<style lang="scss" scoped>

</style>

```



